<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  Created by IntelliJ IDEA.
  User: lc168
  Date: 18/01/2019
  Time: 9:49 AM
  To change this template use File | Settings | File Templates.
--%>


<html>
<head>
    <title>ImageGalleryUI.jsp</title>
</head>
<body>

<%
//String photosPath = (String) request.getAttribute("photosPath");
//List<ImageGalleryDisplay.FileInfo> fileDataList = (List<ImageGalleryDisplay.FileInfo>) request.getAttribute("fileDataList");
%>
<%--<p>--%>
    <%--${fileDataList.size()}--%>
<%--</p>--%>
<table>
    <c:forEach items="${fileDataList}" var="item">
        <c:if test="${not empty item.thumbPath}">
        <tr>
            <td><a href='Photos/${item.fullFile.name}'>
                <img src="Photos/${item.thumbPath.name}" alt="${item.thumbPath}">
                </a>
            </td>
            <td>
                ${item.thumbDisplay}
            </td>
            <td>
                ${item.fullFile}
            </td>
        </tr>
        </c:if>
    </c:forEach>
</table>
</body>
</html>
