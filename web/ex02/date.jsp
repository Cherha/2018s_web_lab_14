<%--
  Created by IntelliJ IDEA.
  User: lc168
  Date: 10/01/2019
  Time: 1:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>date.jsp</title>

    <%@ page import="java.time.LocalDateTime" %>
    <%@ page import="java.time.format.DateTimeFormatter" %>
    <% LocalDateTime time = LocalDateTime.now(); %>
    <% DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY MM dd hh : mm ss"); %>
    <% String result = formatter.format(time); %>
    <p>
        <%= result%>
    </p>

</head>
<body>

</body>
</html>
