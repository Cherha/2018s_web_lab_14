<%--
  Created by IntelliJ IDEA.
  User: lc168
  Date: 10/01/2019
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>exercise01-01.jsp</title>

    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>

    <hr>

    <%@ page import="java.time.LocalDateTime" %>
    <%@ page import="java.time.format.DateTimeFormatter" %>
    <% LocalDateTime time = LocalDateTime.now(); %>
    <% DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY MM dd hh : mm ss"); %>
    <% String result = formatter.format(time); %>
    <p>
        <%= result%>
    </p>

</head>
<body>

</body>
</html>
