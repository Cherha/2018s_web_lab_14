<%@ page import="java.util.Map" %>
<%@ page import="java.util.Map.Entry" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: lc168
  Date: 10/01/2019
  Time: 1:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>survey.jsp</title>
</head>
<body>
    <h1>Server side response</h1>
    <p>Thanks for your submission. The values sent to the server are as follows:</p>
    <%--out.println("<h1>Server side response</h1>");--%>
    <%--out.println("<p>Thanks for your submission. The values sent to the server are as follows:</p>");--%>
    <%
    Map<String, String[]> map = request.getParameterMap();
    Iterator<Entry<String, String[]>> i = map.entrySet().iterator();
    %>
    <table>
        <tbody>
            <%
               while(i.hasNext()) {
                    Entry<String, String[]> entry = i.next();
                    String key = entry.getKey(); //.toUpperCase();
                    String[] values = entry.getValue();

                    if(key.contains("submit") || key.contains("button")) {
                        continue;
                    }
               int index = key.indexOf("[]");
                    if(index != -1) {
                    key = key.substring(0, index);
        }
            %>

            <tr>
                <td><%= key%></td>

            <%
            for(String value: values) {
                out.print("<td>"+ "&lt;" + value + "&gt; " + "</td>");
            }
        %>

            </tr>
        <%
               }
        %>

</body>
</html>
